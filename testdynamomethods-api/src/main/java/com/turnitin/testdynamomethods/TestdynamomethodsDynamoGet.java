package com.turnitin.testdynamomethods;

import com.turnitin.commons.TurnitinContext;
import com.turnitin.commons.db.dynamo.Dao;
import com.turnitin.commons.lambda.ApiGatewayLambda;
import com.turnitin.testdynamomethods.dao.Womp;
import lombok.extern.slf4j.Slf4j;
import software.amazon.awssdk.enhanced.dynamodb.model.BatchWriteResult;
import software.amazon.awssdk.services.dynamodb.model.ConditionalCheckFailedException;

import javax.ws.rs.HttpMethod;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
public class TestdynamomethodsDynamoGet extends ApiGatewayLambda<List<Womp>> {
    static final String DYNAMO_TABLE = "dynamo_table";
    private final Dao dao;
    static String uniqueRun = "";

    // This constructor is for regular flow
    public TestdynamomethodsDynamoGet() {
        this.ctx = TurnitinContext.builder()
                .addEnvironmentVariable(DYNAMO_TABLE)
                .build();
        this.dao = new Dao(ctx.getVariable(DYNAMO_TABLE), Womp.class);
        // Optional: Invoking a simple api here to pre-warm the application
        dao.findByPkAndSk("warmup", "warmup");
        uniqueRun = UUID.randomUUID().toString();
    }


    @Override
    protected List<Womp> handleMethod() throws Exception {

        log.debug("RUNNING TESTS");

        try {
            testGetByPkSk(dao, uniqueRun);
            testExisting(dao, uniqueRun);
            testSaveIfNotExistUniqueField(dao, uniqueRun);
            testdelete(dao, uniqueRun);
            findByDifferentMethods(dao, uniqueRun);
            testWithFilters(dao, uniqueRun);
            updateColumnByDelta(dao, uniqueRun);

            updateColumn(dao, uniqueRun);
            testUpdateItem(dao, uniqueRun);
            queryConditional(dao, uniqueRun);
            queryenhancedrequest(dao, uniqueRun);

            batchwrite(dao, uniqueRun);

        } catch (Exception e) {
            log.error("Test failed", e);
            throw e;
        }

        return Collections.EMPTY_LIST;
    }

    void testGetByPkSk(Dao dao, String uniqueString) throws TestFailedException {
        // Test get if not present
        Womp existingWomp = new Womp();
        existingWomp.setPk("wompAlpha-" + uniqueRun);
        existingWomp.setSk("sk");
        existingWomp.setCreated(System.currentTimeMillis());
        existingWomp.setData("aleph");
        dao.save(existingWomp);

        //It's there...right?
        Optional<Womp> optWomp = dao.findByPkAndSk("wompAlpha-" + uniqueRun, "sk");
        if (optWomp.isEmpty()) throw new TestFailedException();
    }

    void testExisting(Dao dao, String uniqueString) throws TestFailedException {
        // Test get if not present
        Womp existingWomp = new Womp();
        existingWomp.setPk("womp1-" + uniqueRun);
        existingWomp.setSk("sk");
        existingWomp.setCreated(System.currentTimeMillis());
        existingWomp.setData("existing");
        dao.save(existingWomp);

        //bad womp will fail to override existing womp
        Womp badWomp = new Womp();
        badWomp.setPk("womp1-" + uniqueRun);
        badWomp.setSk("sk");
        badWomp.setCreated(System.currentTimeMillis());
        badWomp.setData("badwomp");

        Exception expectedException = null;
        try {
            dao.saveIfNotExist(badWomp);
        } catch (ConditionalCheckFailedException ccfe) {
            expectedException = ccfe;
        }

        //exception should have been thrown
        if (expectedException == null) {
            throw new TestFailedException();
        }

        //make sure original is still in there
        Optional<Womp> answerWomp = dao.findByPkAndSk("womp1-" + uniqueRun, "sk");
        if (answerWomp.isEmpty()) throw new TestFailedException();
        Womp w = answerWomp.get();
        if (!w.getData().equals("existing")) throw new TestFailedException();

        //Now do one where you're not trying to override anything
        Womp goodWomp = new Womp();
        goodWomp.setPk("womp2-" + uniqueRun);
        goodWomp.setSk("sk");
        goodWomp.setCreated(System.currentTimeMillis());
        goodWomp.setData("goodwomp");

        //should not throw exception
        dao.saveIfNotExist(goodWomp);

        //good one is still stored in there right?
        Optional<Womp> answer2womp = dao.findByPkAndSk("womp2-" + uniqueRun, "sk");
        if (answer2womp.isEmpty()) throw new TestFailedException();
        Womp good = answer2womp.get();
        if (!good.getData().equals("goodwomp")) throw new TestFailedException();

    }

    void testSaveIfNotExistUniqueField(Dao dao, String uniqueString) throws TestFailedException {
        Womp existingWomp = new Womp();
        existingWomp.setPk("wwwwahmp-" + uniqueRun);
        existingWomp.setSk("skwamp");
        existingWomp.setCreated(System.currentTimeMillis());
        dao.save(existingWomp);

        Optional<Womp> reallyThere = dao.findByPkAndSk("wwwwahmp-" + uniqueRun, "skwamp");
        if (reallyThere.isEmpty()) throw new TestFailedException();

        Womp newWomp = new Womp();
        newWomp.setPk("wwwwahmp-" + uniqueRun);
        newWomp.setSk("skwamp");
        newWomp.setCreated(System.currentTimeMillis());
        newWomp.setWumpus("womp-womp");

        //should not throw exception
        dao.saveIfNotExist(newWomp, "wumpus");

        Optional<Womp> reallyThere2 = dao.findByPkAndSk("wwwwahmp-" + uniqueRun, "skwamp");
        if (reallyThere2.isEmpty()) throw new TestFailedException();
        if (!reallyThere2.get().getWumpus().equals("womp-womp")) throw new TestFailedException();

        log.info("got to after womp-womp");

        //now make sure if you try to override it fails
        Womp prettyWomp = new Womp();
        prettyWomp.setPk("pretty-" + uniqueRun);
        prettyWomp.setSk("cute");
        prettyWomp.setCreated(System.currentTimeMillis());
        prettyWomp.setWumpus("sopretty");
        dao.save(prettyWomp);

        Womp uglyWomp = new Womp();
        uglyWomp.setPk("pretty-" + uniqueRun);
        uglyWomp.setSk("cute");
        uglyWomp.setCreated(System.currentTimeMillis());
        uglyWomp.setWumpus("sopretty");
        Exception expectedException = null;
        try {
            dao.saveIfNotExist(uglyWomp, "wumpus");
        } catch (ConditionalCheckFailedException ccfe) {
            expectedException = ccfe;
        }
        if (expectedException == null) throw new TestFailedException();
    }

    void testdelete(Dao dao, String uniqueString) throws TestFailedException {

        Womp hipsterWomp = new Womp();
        hipsterWomp.setPk("cool-" + uniqueRun);
        hipsterWomp.setSk("deep-cuts");
        hipsterWomp.setCreated(System.currentTimeMillis());
        hipsterWomp.setWumpus("dubstep");
        dao.save(hipsterWomp);

        Optional<Womp> reallyThere3 = dao.findByPkAndSk("cool-" + uniqueRun, "deep-cuts");
        if (reallyThere3.isEmpty()) throw new TestFailedException();

        boolean flag = dao.delete("cool-" + uniqueRun, "deep-cuts");

        if (!flag) throw new TestFailedException();
        Optional<Womp> reallyThere4 = dao.findByPkAndSk("cool-" + uniqueRun, "deep-cuts");
        if (reallyThere4.isPresent()) throw new TestFailedException();

        //make sure that if you ask it to remove something already removed, it doesn't blow up
        boolean flag2 = dao.delete("cool-" + uniqueRun, "deep-cuts");
        //It only returns flase if an error is thrown
        if (!flag2) throw new TestFailedException();
    }

    void findByDifferentMethods(Dao dao, String uniqueString) throws TestFailedException {
        for (int i = 0; i < 5; i++) {
            Womp groupWomp = new Womp();
            groupWomp.setPk("group-" + uniqueString);
            groupWomp.setSk("groupie-" + i);
            groupWomp.setWumpus("wumpus-" + i);
            dao.save(groupWomp);
        }

        for (int i = 0; i < 7; i++) {
            Womp groupWomp = new Womp();
            groupWomp.setPk("group-" + uniqueString);
            groupWomp.setSk("band-" + i);
            groupWomp.setWumpus("wumpus-" + i);
            dao.save(groupWomp);
        }

        List<Womp> wompList = dao.findAllByPk("group-" + uniqueString);
        if (wompList.size() != 12) throw new TestFailedException();

        Optional<Womp> oneWomp = dao.findOneByPk("group-" + uniqueString);
        if (oneWomp.isEmpty()) throw new TestFailedException();
        if (oneWomp.get().getWumpus() == null) throw new TestFailedException();

        List<Womp> wompBand = dao.findAllByPkAndSkPrefix("group-" + uniqueString, "band");
        log.info("WompBand found " + wompBand.size() + " entries");
        if (wompBand.size() != 7) throw new TestFailedException();

        Optional<Womp> wompLeadSinger = dao.findOneByPkAndSkPrefix("group-" + uniqueString, "band");
        log.info("WompLeadSinger found " + wompBand.size() + " entries");
        if (wompLeadSinger.isEmpty()) throw new TestFailedException();

    }

    void testWithFilters(Dao dao, String uniqueString) throws TestFailedException{
        String pk = "wild-" + uniqueString;
        //insert 15 wumpuses

        for (int i = 0; i < 5; i++) {
            Womp wildWumpus = new Womp();
            wildWumpus.setPk(pk);
            wildWumpus.setSk("skprefix-wild" + i + "-" + uniqueString);
            wildWumpus.setWumpus("wild-" + i);
            wildWumpus.setIntWumpus(100+i);   // 101, 102 .. 115
            wildWumpus.setLongWumpus(1000 + i);
            dao.save(wildWumpus);
        }

        for (int i = 0; i < 5; i++) {
            Womp tameWumpus = new Womp();
            tameWumpus.setPk(pk);
            tameWumpus.setSk("skprefix-tame" + i + "-" + uniqueString);
            tameWumpus.setWumpus("tame-" + i);
            tameWumpus.setIntWumpus(100+i);   // 101, 102 .. 115
            tameWumpus.setLongWumpus(1000 + i);
            dao.save(tameWumpus);
        }

        for (int i = 0; i < 5; i++) {
            Womp hairyWumpus = new Womp();
            hairyWumpus.setPk(pk);
            hairyWumpus.setSk("skprefix-hairy" + i + "-" + uniqueString);
            hairyWumpus.setWumpus("wild-" + i);
            hairyWumpus.setIntWumpus(100+i);   // 101, 102 .. 115
            hairyWumpus.setLongWumpus(1000 + i);
            dao.save(hairyWumpus);
        }

        for (int i = 0; i < 5; i++) {
            Womp weirdWumpus = new Womp();
            weirdWumpus.setPk(pk);
            weirdWumpus.setSk("skweirdPrefix" + i + "-" + uniqueString);
            weirdWumpus.setWumpus("weird-" + i);
            weirdWumpus.setIntWumpus(100+i);   // 101, 102 .. 115
            weirdWumpus.setLongWumpus(2000 + i);  //two thousand
            dao.save(weirdWumpus);
        }

        List<Womp> womplistInt = dao.findAllByPkAndSkPrefixWithFilter(pk, "skprefix-", "intWumpus", 102);
        if(womplistInt.size()!=3) throw new TestFailedException();

        List<Womp> womplistInt2 = dao.findAllByPkAndSkPrefixWithFilter(pk, "skweird", "intWumpus", 103);
        if(womplistInt2.size()!=1) throw new TestFailedException();

        List<Womp> womplistInt3 = dao.findAllByPkAndSkPrefixWithFilter(pk, "skprefix-", "intWumpus", 155);
        if(!womplistInt3.isEmpty()) throw new TestFailedException();

        List<Womp> womplistInt4 = dao.findAllByPkAndSkPrefixWithFilter(pk, "sk", "intWumpus", 101);
        if(womplistInt4.size()!=4) throw new TestFailedException();


        Optional<Womp> wompInt = dao.findOneByPkAndSkPrefixWithFilter(pk, "sk", "intWumpus", 101);
        if(wompInt.isEmpty()) throw new TestFailedException();

        Optional<Womp> wompInt2 = dao.findOneByPkAndSkPrefixWithFilter(pk, "sk", "intWumpus", 30000);
        if(wompInt2.isPresent()) throw new TestFailedException();



        List<Womp> womplistlong = dao.findAllByPkAndSkPrefixWithFilter(pk, "skprefix-", "longWumpus", Long.valueOf(1000));
        log.info("size is: " + womplistlong.size());
        if(womplistlong.size()!=3) throw new TestFailedException();

        List<Womp> womplistlong2 = dao.findAllByPkAndSkPrefixWithFilter(pk, "skweird", "longWumpus", Long.valueOf(2004));
        if(womplistlong2.size()!=1) throw new TestFailedException();

        List<Womp> womplistlong3 = dao.findAllByPkAndSkPrefixWithFilter(pk, "skprefix-", "longWumpus", Long.valueOf(3005));
        if(!womplistlong3.isEmpty()) throw new TestFailedException();

        List<Womp> womplistlong4 = dao.findAllByPkAndSkPrefixWithFilter(pk, "sk", "longWumpus", Long.valueOf(1003));
        log.info("size is: " + womplistlong4.size());
        if(womplistlong4.size()!=3) throw new TestFailedException();

        Optional<Womp> longWomp  = dao.findOneByPkAndSkPrefixWithFilter(pk, "sk", "longWumpus", Long.valueOf(1003));
        if(longWomp.isEmpty()) throw new TestFailedException();

        Optional<Womp> longWomp2  = dao.findOneByPkAndSkPrefixWithFilter(pk, "sk", "longWumpus", Long.valueOf(6003));
        if(longWomp2.isPresent()) throw new TestFailedException();



        List<Womp> womplistString = dao.findAllByPkAndSkPrefixWithFilter(pk, "skprefix-", "wumpus", "wild-1");
        if(womplistString.size()!=2) throw new TestFailedException();

        List<Womp> womplistString2 = dao.findAllByPkAndSkPrefixWithFilter(pk, "skweird", "wumpus", "weird-3");
        if(womplistString2.size()!=1) throw new TestFailedException();

        List<Womp> womplistString3 = dao.findAllByPkAndSkPrefixWithFilter(pk, "skprefix-", "wumpus", "examsoft");
        if(!womplistString3.isEmpty()) throw new TestFailedException();

        Optional<Womp> stringWomp  = dao.findOneByPkAndSkPrefixWithFilter(pk, "sk", "wumpus", "wild-1");
        if(stringWomp.isEmpty()) throw new TestFailedException();

        Optional<Womp> stringWomp2 = dao.findOneByPkAndSkPrefixWithFilter(pk, "sk", "wumpus", "turnitin");
        if(stringWomp2.isPresent()) throw new TestFailedException();

        List<Womp> allList = dao.findAllByPk(pk);
        if(allList.size()!=20) throw new TestFailedException();

        List<Womp> noList = dao.findAllByPk("stuff");
        if(!noList.isEmpty()) throw new TestFailedException();
    }

    void updateColumnByDelta(Dao dao, String uniqueString) throws TestFailedException {
        Womp wam = new Womp();
        wam.setPk("delta");
        wam.setSk("sk-delta");
        wam.setIntWumpus(55);
        wam.setLongWumpus(22);
        dao.save(wam);

         dao.updateColumnByDelta("delta", "sk-delta", "intWumpus", -4);
        Optional<Womp> wam1 = dao.findByPkAndSk("delta", "sk-delta");
        if(wam1.get().getIntWumpus()!=51) throw new TestFailedException();

        dao.updateColumnByDelta("delta", "sk-delta", "longWumpus", 4);
        Optional<Womp> wam2 = dao.findByPkAndSk("delta", "sk-delta");
        if(wam2.get().getLongWumpus()!=26) throw new TestFailedException();

        dao.updateColumnByDelta("pk","delta", "sk", "sk-delta", "intWumpus", -3);
        Optional<Womp> wam3 = dao.findByPkAndSk("delta", "sk-delta");
        if(wam3.get().getIntWumpus()!=48) throw new TestFailedException();

        dao.updateColumnByDelta("pk","delta","sk", "sk-delta", "longWumpus", 5);
        Optional<Womp> wam4 = dao.findByPkAndSk("delta", "sk-delta");
        if(wam4.get().getLongWumpus()!=31) throw new TestFailedException();

    }


    void updateColumn(Dao dao, String uniqueString) {

        /**
         * public void updateColumn(final String pkName, final String pk, final String skName, final String sk, final String columnName,
         * 							 final int newIntegerValue) {
         */

        /**
         * public void updateColumn(final String pk, final String sk, final String columnName, final int newIntegerValue) {
         * 		updateColumn("pk", pk, "sk", sk, columnName, AttributeValues.numberValue(newIntegerValue), AttributeAction.PUT);
         *
         */

        /**
         * public void updateColumn(final String pkName, final String pk, final String skName, final String sk, final String columnName,
         * 							 final long newLongValue) {
         * 		updateColumn(pkName, pk, skName, sk, columnName, AttributeValues.numberValue(newLongValue), AttributeAction.PUT);
         *        }
         */

        /**
         * 	public void updateColumn(final String pk, final String sk, final String columnName, final long newLongValue) {
         * 		updateColumn("pk", pk, "sk", sk, columnName, AttributeValues.numberValue(newLongValue), AttributeAction.PUT);
         *        }
         */

        /**
         * public void updateColumn(final String pkName, final String pk, final String skName, final String sk, final String columnName,
         * 							 final String newStringValue) {
         * 		updateColumn(pkName, pk, skName, sk, columnName, AttributeValues.stringValue(newStringValue), AttributeAction.PUT);
         */

        /**
         * public void updateColumn(final String pk, final String sk, final String columnName, final String newStringValue) {
         * 		updateColumn("pk", pk, "sk", sk, columnName, AttributeValues.stringValue(newStringValue), AttributeAction.PUT);
         *        }
         */
    }

    void testUpdateItem(Dao dao, String uniqueString) {

    }

    void queryConditional(Dao dao, String uniqueString) {

    }

    void queryenhancedrequest(Dao dao, String uniqueString) {

    }

    void batchwrite(Dao dao, String uniqueString) throws TestFailedException, Exception{
        List<Womp> allList = dao.findAllByPk("wild-" + uniqueString);

        for(Womp w:allList){
            w.setLongWumpus(0);
            w.setIntWumpus(0);
        }
        List<Womp> toDelete = new ArrayList<>();
        BatchWriteResult result = dao.batchWrite(allList, toDelete);


    }


    @Override
    protected List<String> getSupportedHttpMethods() {
        return Arrays.asList(HttpMethod.GET);
    }

    public class TestFailedException extends Exception {
        public TestFailedException(){
            super("One of checks failed");
        }
    }

}
