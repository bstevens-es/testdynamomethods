package com.turnitin.testdynamomethods;

import com.turnitin.testdynamomethods.dao.Womp;
import com.turnitin.commons.TurnitinContext;
import com.turnitin.commons.db.dynamo.Dao;
import com.turnitin.commons.lambda.ApiGatewayLambda;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.HttpMethod;
import java.util.Arrays;
import java.util.List;

@Slf4j
public class TestdynamomethodsDynamoPost extends ApiGatewayLambda<Womp> {
	static final String DYNAMO_TABLE = "dynamo_table";

	private final Dao dao;

	// This constructor is for regular flow
	public TestdynamomethodsDynamoPost() {
		this.ctx = TurnitinContext.builder()
				.addEnvironmentVariable(DYNAMO_TABLE)
				.build();
		this.dao = new Dao(ctx.getVariable(DYNAMO_TABLE), Womp.class);
		// Optional: Invoking a simple api here to pre-warm the application
		dao.findByPkAndSk("warmup", "warmup");
	}

	// This Constructor is use in tests if you want to mock the context or parts there of.
	public TestdynamomethodsDynamoPost(TurnitinContext ctx, Dao mockedDao) {
		this.ctx = ctx;
		this.dao = mockedDao;
	}

	@Override
	protected Womp handleMethod() throws Exception {
		log.info("input path: {}", input.getPath());

		String dbkey = ctx.getKeyFromPathParams(input, "dbkey");
		String value = ctx.getKeyFromPathParams(input, "value");
		log.debug("dbkey: {}, value: {}", dbkey, value);

		if (dbkey != null && !dbkey.isEmpty()) {

			log.debug("POST body: {}", input.getBody());
			String body = input.getBody();
			Womp simpleRecord = Womp.builder()
					.pk(dbkey)
					.sk(value)
					.created(System.currentTimeMillis())
					.data(body)
					.build();
			dao.save(simpleRecord);
			return simpleRecord;
		} else {
			throw new Exception("unsupported query");
		}
	}

	@Override
	protected List<String> getSupportedHttpMethods() {
		return Arrays.asList(HttpMethod.POST);
	}


}
