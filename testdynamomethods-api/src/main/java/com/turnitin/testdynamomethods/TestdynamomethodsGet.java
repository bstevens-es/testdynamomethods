package com.turnitin.testdynamomethods;

import com.turnitin.testdynamomethods.model.Hello;
import com.turnitin.testdynamomethods.service.TestdynamomethodsService;
import com.turnitin.commons.TurnitinContext;
import com.turnitin.commons.lambda.ApiGatewayLambda;
import java.util.Arrays;
import java.util.List;
import javax.ws.rs.HttpMethod;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestdynamomethodsGet extends ApiGatewayLambda<Hello> {

    private TestdynamomethodsService testdynamomethodsservice;

    // This constructor is for regular flow
    public TestdynamomethodsGet() {
        this.ctx = TurnitinContext.builder().build();
        this.testdynamomethodsservice = new TestdynamomethodsService(ctx);
    }

    // This Constructor is use in tests if you want to mock the context or parts there of.
    public TestdynamomethodsGet(TurnitinContext ctx) {
        this.ctx = ctx;
        this.testdynamomethodsservice = new TestdynamomethodsService(ctx);
    }

    @Override
    protected Hello handleMethod() throws Exception {
        String who = ctx.getKeyFromPathParams(input, "who");
        return testdynamomethodsservice.doHello(who);
    }

    @Override
    protected List<String> getSupportedHttpMethods() {
        return Arrays.asList(HttpMethod.GET);
    }
}
