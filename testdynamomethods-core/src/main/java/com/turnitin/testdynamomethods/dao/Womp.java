package com.turnitin.testdynamomethods.dao;

import com.turnitin.commons.db.dynamo.Persistable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbBean;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbPartitionKey;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbSortKey;


@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@DynamoDbBean
public class Womp implements Persistable{
   private String pk;
   private String sk;
   private long created;
   private String data;
   // A Wild Wumpus has appeared!!
   private String wumpus;
   private long longWumpus;
   private int intWumpus;

   @DynamoDbPartitionKey
   public String getPk() {
      return this.pk;
   }

   public void setPk(String pk) {
      this.pk = pk;
   }

   @DynamoDbSortKey
   public String getSk() {
      return this.sk;
   }

   public void setSk(String sk) {
      this.sk = sk;
   }

   public long getCreated() {
      return created;
   }

   @Override
   public void setCreated(long created) {
      this.created = created;
   }

   public String getData() {
      return this.data;
   }

   public void setData(String data) {
      this.data = data;
   }

   public String getWumpus() {
      return wumpus;
   }

   public void setWumpus(String wumpus) {
      this.wumpus = wumpus;
   }

   public long getLongWumpus() {
      return longWumpus;
   }

   public void setLongWumpus(long longWumpus) {
      this.longWumpus = longWumpus;
   }

   public int getIntWumpus() {
      return intWumpus;
   }

   public void setIntWumpus(int intWumpus) {
      this.intWumpus = intWumpus;
   }
}
