package com.turnitin.testdynamomethods.service;

import com.turnitin.testdynamomethods.model.Hello;
import com.turnitin.commons.TurnitinContext;

// This is the kind of server in which the business logic might go
public class TestdynamomethodsService {

	TurnitinContext ctx;

	public TestdynamomethodsService(TurnitinContext ctx) {
		this.ctx = ctx;
	}

	public Hello doHello(String who) {
		Hello hello = new Hello();
		hello.setResponse("Hello " + who);
		return hello;
	}

}
